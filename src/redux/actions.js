import {
  ACTIVATE_ICON,
  ADD_CATEGORY,
  // AUTORIZATION,
  // COMPLETED_TASK,
  CREATE_TASK,
  DEACTIVATE_ICON,
  DELETE_TASK,
  DEL_CATEGORY,
  HIDE_ADD_FORM,
  HIDE_CATEGORY_FORM,
  HIDE_EDIT_FORM,
  HIDE_EDIT_FORM_CATEGORY,
  LOGIN,
  LOGOUT,
  SET_ACTIVE_CATEGORY,
  SHOW_ADD_FORM,
  SHOW_CATEGORY_FORM,
  SHOW_EDIT_FORM,
  SHOW_EDIT_FORM_CATEGORY,
  UPDATE_CATEGORY,
  // UNCOMPLETED_TASK,
  UPDATE_TASK,
} from "./Types";

export const createTask = (task) => {
  return {
    type: CREATE_TASK,
    payload: task,
  };
};

export const showEditForm = (id) => {
  return {
    type: SHOW_EDIT_FORM,
    payload: id,
  };
};
export const hideEditForm = () => {
  return {
    type: HIDE_EDIT_FORM,
  };
};
export const showAddForm = () => {
  return {
    type: SHOW_ADD_FORM,
  };
};
export const hideAddForm = () => {
  return {
    type: HIDE_ADD_FORM,
  };
};

export const updateTask = (id, params) => {
  return {
    type: UPDATE_TASK,
    payload: params,
    id,
  };
};

export const deleteTask = (id) => {
  // dispatch(hideEditForm());
  return { type: DELETE_TASK, id };
};

export const showAddCategotyForm = () => {
  return { type: SHOW_CATEGORY_FORM };
};
export const HideAddCategotyForm = () => {
  return { type: HIDE_CATEGORY_FORM };
};

export const addCategory = (сategory) => {
  return { type: ADD_CATEGORY, payload: сategory };
};
export const delCategory = (id) => {
  return { type: DEL_CATEGORY, id };
};
export const setActiveCategory = (id) => {
  return { type: SET_ACTIVE_CATEGORY, payload: id };
};

export const setActiveIcon = (iconName) => {
  return { type: ACTIVATE_ICON, payload: iconName };
};

export const deactivateIcon = (iconName) => {
  return { type: DEACTIVATE_ICON, payload: iconName };
};

export const showEditFormCategory = () => {
  return { type: SHOW_EDIT_FORM_CATEGORY };
};
export const hideEditFormCategory = () => {
  return { type: HIDE_EDIT_FORM_CATEGORY };
};

export const updateCategory = (name) => {
  return { type: UPDATE_CATEGORY, payload: name };
};

export const loginUser = (userInfo) => {
  return { type: LOGIN, payload: userInfo };
};
export const logoutUser = (userInfo) => {
  return { type: LOGOUT, payload: userInfo };
};
// export const autorization = (userInfo) => {
//   return { type: AUTORIZATION, payload: userInfo };
// };

// export const completedTask = (id) => {
//   return {
//     type: COMPLETED_TASK,
//     id,
//   };
// };
// export const unCompletedTask = (id) => {
//   return {
//     type: UNCOMPLETED_TASK,
//     id,
//   };
// };
