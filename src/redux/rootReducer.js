import {
  checkUser,
  loadLocalStorageCategory,
  loadLocalStorageData,
  setLocalStorageCategory,
  setLocalStorageData,
} from "../components/LocalStorage";
import {
  ACTIVATE_ICON,
  ADD_CATEGORY,

  // COMPLETED_TASK,
  CREATE_TASK,
  DEACTIVATE_ICON,
  DELETE_TASK,
  DEL_CATEGORY,
  HIDE_ADD_FORM,
  HIDE_CATEGORY_FORM,
  HIDE_EDIT_FORM,
  HIDE_EDIT_FORM_CATEGORY,
  LOGIN,
  LOGOUT,
  SET_ACTIVE_CATEGORY,
  SHOW_ADD_FORM,
  SHOW_CATEGORY_FORM,
  SHOW_EDIT_FORM,
  SHOW_EDIT_FORM_CATEGORY,
  UPDATE_CATEGORY,
  UPDATE_TASK,
} from "./Types";

export const users = [
  { login: "ira", password: "12345" },
  { login: "test", password: "54321" },
];

const initialState = {
  tasks: loadLocalStorageData(),
  editTaskId: null,
  isEditAddForm: false,
  isCategoryForm: false,
  catList: loadLocalStorageCategory() || [{ catId: 0, name: "Без категории" }],
  currentCat: 0,
  activeIcons: [],
  currentUser: null,
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_TASK:
      const currentTasks = [...state.tasks, action.payload];
      setLocalStorageData(currentTasks);
      return {
        ...state,
        tasks: currentTasks,
      };
    case SHOW_EDIT_FORM:
      return {
        ...state,
        editTaskId: action.payload,
      };
    case HIDE_EDIT_FORM:
      return {
        ...state,
        editTaskId: null,
      };

    case UPDATE_TASK:
      const tasks = state.tasks.map((task) => {
        return task.id === action.id ? { ...task, ...action.payload } : task;
      });
      // console.log(tasks);
      setLocalStorageData(tasks);
      return {
        ...state,
        tasks,
      };
    case SHOW_ADD_FORM:
      return { ...state, isAddForm: true };
    case HIDE_ADD_FORM:
      return { ...state, isAddForm: false };
    case DELETE_TASK:
      let newTasks = state.tasks.filter(
        (task) => !(task.id === action.id && task.isCompleted)
      );
      setLocalStorageData(newTasks);
      return {
        ...state,
        tasks: newTasks,
        editTaskId: null,
      };
    case SHOW_CATEGORY_FORM:
      return { ...state, isCategoryForm: true };
    case HIDE_CATEGORY_FORM:
      return { ...state, isCategoryForm: false };
    case ADD_CATEGORY:
      const id = Math.max(...state.catList.map((cat) => cat.catId)) + 1;
      const newCatList = [
        ...state.catList,
        { name: action.payload, catId: id },
      ];
      // console.log(newCatList);
      setLocalStorageCategory(newCatList);
      return { ...state, catList: newCatList };

    case DEL_CATEGORY:
      const catListAfterDel = state.catList.filter(
        (cat) => action.id !== cat.catId
      );

      setLocalStorageCategory(catListAfterDel);
      return {
        ...state,
        catList: catListAfterDel,
        currentCat: 0,
      };
    case SET_ACTIVE_CATEGORY:
      return { ...state, currentCat: action.payload, activeIcons: [] };

    case SHOW_EDIT_FORM_CATEGORY:
      return { ...state, isEditFormCategory: true };
    case HIDE_EDIT_FORM_CATEGORY:
      return { ...state, isEditFormCategory: false };
    case UPDATE_CATEGORY:
      const categories = state.catList.map((cat) => {
        return cat.catId === state.currentCat
          ? { ...cat, name: action.payload }
          : cat;
      });

      setLocalStorageCategory(categories);
      return { ...state, catList: categories };
    case ACTIVATE_ICON:
      return { ...state, activeIcons: [...state.activeIcons, action.payload] };
    case DEACTIVATE_ICON:
      return {
        ...state,
        activeIcons: state.activeIcons.filter(
          (icon) => icon !== action.payload
        ),
      };
    case LOGIN:
      const userInfo = checkUser(action.payload) ? action.payload : null;
      console.log(userInfo);
      return {
        ...state,
        // auth:true,
        currentUser: userInfo,
      };
    case LOGOUT:
      return {
        ...state,
        // auth:false,
        currentUser: null,
      };
    // case COMPLETED_TASK:
    //   // const tasks = state.tasks.map((task) => {
    //   //   return task.id === action.id ? { ...task, isCompleted: true } : task;
    //   // });
    //   return { ...state, tasks };
    default:
      return state;
  }
};
