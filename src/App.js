import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import AutorizationForm from "./components/AutorizationForm";
import Error from "./components/Error";
import Todos from "./components/Todos";

const App = (props) => {
  const { currentUser } = props;

  return (
    <div className="App">
      <Switch>
        {!currentUser ? (
          <Route exact path="/" component={AutorizationForm}></Route>
        ) : (
          <Route exact path="/" component={Todos}></Route>
        )}
        {currentUser && <Route path="/todos/:id" component={Todos}></Route>}
        <Route component={Error}></Route>
      </Switch>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
  };
};

export default connect(mapStateToProps)(App);
