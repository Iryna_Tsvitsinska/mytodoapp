import { users } from "../redux/rootReducer";

export function setLocalStorageData(tasks) {
  return localStorage.setItem("tasks", JSON.stringify(tasks));
}

export function loadLocalStorageData() {
  return localStorage.hasOwnProperty("tasks")
    ? JSON.parse(localStorage.getItem("tasks"))
    : [];
}

export function setLocalStorageCategory(categories) {
  return localStorage.setItem("categories", JSON.stringify(categories));
}

export function loadLocalStorageCategory() {
  return localStorage.hasOwnProperty("categories")
    ? JSON.parse(localStorage.getItem("categories"))
    : [{ catId: 0, name: "Без категории" }];
}

export function checkUser(userInfo) {
  const user = users.find(
    (user) =>
      userInfo.login === user.login && userInfo.password === user.password
  );
  return user;
}
