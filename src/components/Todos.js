import AddTaskForm from "./AddTaskForm";
import ListTasks from "./ListTasks";
import EditForm from "./EditForm";
import { connect, useDispatch } from "react-redux";
import {
  deactivateIcon,
  delCategory,
  logoutUser,
  setActiveCategory,
  setActiveIcon,
  showAddCategotyForm,
  showAddForm,
  showEditFormCategory,
} from "../redux/actions";
import AddCategoryForm from "./AddCategoryForm";
import EditFormCategory from "./EditFormCategory";
import { iconsName } from "./IconPicker";
import { Link } from "react-router-dom";

function Todos(props) {
  const dispatch = useDispatch();

  const renderCategories = (currentCat) => {
    return (
      <div>
        <div className="tabs is-centered is-large">
          <ul>
            {props.catList.map((cat) => (
              <li
                className={`${currentCat === cat.catId && "is-active"}`}
                key={cat.catId}
                onClick={() => dispatch(setActiveCategory(cat.catId))}
              >
                <Link to={`/todos/${cat.catId}`}>{cat.name}</Link>
              </li>
            ))}
          </ul>
        </div>

        {renderProgressBarCategory(currentCat)}
      </div>
    );
  };
  const iconToggleClick = (icon) => {
    props.activeIcons.includes(icon)
      ? dispatch(deactivateIcon(icon))
      : dispatch(setActiveIcon(icon));
  };
  const renderIcons = (icons) => {
    return (
      <div className="buttons has-addons is-centered">
        {icons.map((icon) => (
          <button
            key={icon}
            className={`m-2 p-5 button ${
              props.activeIcons.includes(icon) && "is-info"
            }`}
            onClick={() => iconToggleClick(icon)}
          >
            <span className="icon">
              <i className={icon}></i>
            </span>
            <span className="icon">{getTaskAmountWithIcon(icon)}</span>
          </button>
        ))}
      </div>
    );
  };

  const getTaskAmountWithIcon = (icon) => {
    const { currentCat, tasks } = props;
    const iconsAmount = tasks.filter(
      (task) => task.catId === currentCat && task.icon === icon
    ).length;
    return iconsAmount ? iconsAmount : "";
  };

  const deleteCategoryHandler = () => {
    const taskslength = props.tasks.filter(
      (task) => task.catId === props.currentCat
    ).length;
    taskslength
      ? alert(`Невозможно удалить. Задач в каталоге: ${taskslength} `)
      : dispatch(delCategory(props.currentCat));
  };

  const renderProgressBarCategory = (currentCat) => {
    const { tasks } = props;
    const amountDoneTask = tasks.filter(
      (task) => task.catId === currentCat && task.isCompleted
    ).length;
    const amountTasksByCategory = tasks.filter(
      (task) => task.catId === currentCat
    ).length;

    const progress = amountTasksByCategory
      ? Math.round((amountDoneTask * 100) / amountTasksByCategory)
      : 0;
    return (
      <progress
        className="progress is-info"
        value={progress}
        max="100"
      ></progress>
    );
  };

  return (
    <div className="App">
      <div className="p-5 has-background-primary-light">
        <div className="block">
          <button
            className="button is-warning"
            onClick={() => dispatch(showAddForm())}
          >
            Новая задача
          </button>
          <button
            className="button is-success"
            onClick={() => dispatch(showAddCategotyForm())}
          >
            Добавить категорию
          </button>
          <button
            className="button is-info"
            onClick={() => dispatch(showEditFormCategory())}
          >
            Редактировать категорию
          </button>
          <button className="button is-danger" onClick={deleteCategoryHandler}>
            Удалить категорию
          </button>
          <button
            className="button is-warning"
            onClick={() => dispatch(logoutUser())}
          >
            Выйти
          </button>
        </div>
        <div className="block">{renderCategories(props.currentCat)}</div>

        {renderIcons(iconsName)}
      </div>
      {props.isEditFormCategory && <EditFormCategory />}
      {props.isAddForm && <AddTaskForm />}
      {props.editTask && <EditForm />}
      {props.isCategoryForm && <AddCategoryForm />}
      <ListTasks />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    editTask: state.editTaskId,
    isAddForm: state.isAddForm,
    isCategoryForm: state.isCategoryForm,
    catList: state.catList,
    currentCat: state.currentCat,
    tasks: state.tasks,
    isEditFormCategory: state.isEditFormCategory,
    activeIcons: state.activeIcons,
  };
};
export default connect(mapStateToProps)(Todos);
