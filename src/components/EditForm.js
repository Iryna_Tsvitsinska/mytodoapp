import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { deleteTask, hideEditForm, updateTask } from "../redux/actions";
import { ColorPicker } from "./ColorPicker";
import { IconPicker } from "./IconPicker";

const EditForm = ({ currentTask }) => {
  const [task, setTask] = useState({
    title: currentTask.title,
    description: currentTask.description,
    color: currentTask.color,
    icon: currentTask.icon,
  });
  console.log(currentTask);
  const dispatch = useDispatch();

  const changeInputHandler = (event) => {
    setTask((prev) => ({
      ...prev,
      ...{ [event.target.name]: event.target.value },
    }));
  };

  const saveEditHandler = () => {
    console.log(task);
    dispatch(
      updateTask(currentTask.id, {
        title: task.title,
        description: task.description,
        color: task.color,
        icon: task.icon,
      })
    );
    dispatch(hideEditForm());
  };

  function changeColorHandler(nextColor) {
    setTask((prev) => {
      return { ...prev, color: nextColor };
    });
  }

  function changeIconHandler(icon) {
    setTask((prev) => {
      return { ...prev, icon };
    });
  }

  function hasChanges() {
    return (
      task.title !== currentTask.title ||
      task.description !== currentTask.description ||
      task.color !== currentTask.color ||
      task.icon !== currentTask.icon
    );
  }

  return (
    currentTask && (
      <div className={`modal is-active`}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head is-justify-content-space-between">
            <p className="modal-card-title">Редактирование задачи</p>
            <button
              className="delete is-large"
              onClick={() => dispatch(hideEditForm())}
              aria-label="close"
            ></button>
          </header>
          <section className="modal-card-body">
            <label className="label">Название задачи</label>
            <input
              className={`input ${!task.title.trim() && "is-danger"}`}
              name="title"
              type="text"
              value={task.title}
              onChange={changeInputHandler}
              autoFocus
            ></input>
            {!task.title.trim() && (
              <p className="help is-danger">Поле не может быть пустым</p>
            )}
            <label className="mt-5 label">Описание задачи</label>
            <textarea
              className="textarea mt-3"
              name="description"
              rows="3"
              onChange={changeInputHandler}
              value={task.description}
            ></textarea>
            {/* <input
              className="input mt-3"
              name="description"
              type="text"
              value={task.description}
              onChange={changeInputHandler}
            /> */}
            <IconPicker
              currentIcon={task.icon}
              changeIcon={changeIconHandler}
              color={task.color}
            />
            <ColorPicker
              currentColor={task.color}
              changeColor={changeColorHandler}
            />
          </section>
          <footer className="modal-card-foot columns is-justify-content-space-between">
            <div>
              <button
                className={`${
                  hasChanges()
                    ? "button is-success is-start"
                    : "button is-start"
                }`}
                onClick={saveEditHandler}
                disabled={!hasChanges()}
              >
                Сохранить
              </button>
              <button
                className="button is-start"
                onClick={() => dispatch(hideEditForm())}
              >
                Отменить
              </button>
            </div>
            <div>
              <button
                className="button is-danger is-right "
                onClick={() => dispatch(deleteTask(currentTask.id))}
                disabled={!currentTask.isCompleted}
              >
                Удалить
              </button>
            </div>
          </footer>
        </div>
      </div>
    )
  );
};
const editTaskById = (tasks, id) => {
  return tasks.find((task) => task.id === id);
};

const mapStateToProps = (state) => {
  const currentTask = editTaskById(state.tasks, state.editTaskId);

  return {
    currentTask: currentTask ? currentTask : null,
  };
};

// const mapDispatchToProps = {
//   deleteTask,
// };

export default connect(mapStateToProps)(EditForm);
