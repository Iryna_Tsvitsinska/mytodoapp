import { connect } from "react-redux";
import { Task } from "./Task";

const ListTasks = ({ tasks, currentCat, activeIcons }) => {
  if (tasks.length === 0) {
    return <p className="has-text-centered">заданий пока нет</p>;
  }

  function sortTasks(currentTasks) {
    const completedTasks = [];
    const unCompletedTasks = [];
    const filterTasks = activeIcons.length
      ? currentTasks.filter(
          (task) => currentCat === task.catId && activeIcons.includes(task.icon)
        )
      : currentTasks.filter((task) => currentCat === task.catId);

    filterTasks.forEach((task) => {
      task.isCompleted
        ? completedTasks.push(task)
        : unCompletedTasks.push(task);
    });

    return { completedTasks, unCompletedTasks };
  }
  const { completedTasks, unCompletedTasks } = sortTasks(tasks);
  return (
    <div>
      {unCompletedTasks && (
        <ul className="m-3">
          {unCompletedTasks.map((task) => (
            <li key={task.id}>
              <div className="box m-1 has-background-info-light">
                <Task task={task} />
              </div>
            </li>
          ))}
        </ul>
      )}
      {completedTasks && (
        <ul className="m-3">
          {completedTasks.map((task) => (
            <li key={task.id}>
              <div className="box m-1 has-background-primary-light">
                <Task task={task} />
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks,
    currentCat: state.currentCat,
    activeIcons: state.activeIcons,
  };
};

export default connect(mapStateToProps)(ListTasks);
