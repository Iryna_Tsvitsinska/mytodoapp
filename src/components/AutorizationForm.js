import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { loginUser } from "../redux/actions";
import { checkUser } from "./LocalStorage";

export const AutorizationForm = () => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    login: "",
    password: "",
    isNotCorrect: false,
  });

  const changeInputHandler = (event) => {
    setState((prev) => ({
      ...prev,
      ...{ [event.target.name]: event.target.value },
      isNotCorrect: false,
    }));
  };

  const loginHandler = () => {
    checkUser({ login: state.login, password: state.password })
      ? dispatch(loginUser({ login: state.login, password: state.password }))
      : setState({
          login: "",
          password: "",
          isNotCorrect: true,
        });
  };

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Форма авторизации</p>
        </header>
        <section className="modal-card-body">
          <label className="label">Логин</label>
          <input
            className={` input ${state.isNotCorrect && "is-danger"}`}
            name="login"
            type="text"
            onChange={changeInputHandler}
            value={state.login}
          ></input>
          <label className="label">Пароль</label>
          <input
            className={` input ${state.isNotCorrect && "is-danger"}`}
            name="password"
            type="password"
            onChange={changeInputHandler}
            value={state.password}
          ></input>
          {state.isNotCorrect && (
            <p className="has-text-danger">Неправельный логин или пароль</p>
          )}
        </section>
        <footer className="modal-card-foot">
          <button className="button is-success" onClick={loginHandler}>
            Авторизироваться
          </button>
        </footer>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
  };
};

export default connect(mapStateToProps)(AutorizationForm);
