import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { hideEditFormCategory, updateCategory } from "../redux/actions";

const EditFormCategory = ({ nameCategory, catList }) => {
  const [state, setState] = useState({ name: nameCategory });
  const dispatch = useDispatch();

  const changeInputHandler = (event) => {
    setState((prev) => ({
      ...prev,
      ...{ name: event.target.value },
    }));
  };
  const updateCategoryHandler = () => {
    if (checkIncludesCategory(state.name, catList)) {
      alert("Категория уже существует");
    } else {
      dispatch(updateCategory(state.name.trim()));
      dispatch(hideEditFormCategory());
    }
  };

  const checkIncludesCategory = (catName, catList) => {
    return catList.find((cat) => cat.name === catName);
  };

  const cancelChanges = () => {
    setState({ name: nameCategory });
  };

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Редактировать категорию</p>
          <button
            className="delete is-large"
            aria-label="close"
            onClick={() => dispatch(hideEditFormCategory())}
          ></button>
        </header>
        <section className="modal-card-body">
          <label className="label">Название категории</label>
          <input
            className={`input ${!state.name.trim() && "is-danger"}`}
            value={state.name}
            onChange={changeInputHandler}
          ></input>
          {!state.name.trim() && (
            <p className="has-text-danger">
              Категорию нельзя сохранить без названия
            </p>
          )}
          {checkIncludesCategory(state.name, catList) &&
            state.name !== nameCategory && (
              <p className="has-text-danger">
                {`Категория "${state.name}" уже существует`}
              </p>
            )}
        </section>
        <footer className="modal-card-foot">
          <button
            className={state.name.trim() ? "button is-success" : "button"}
            disabled={
              !state.name.trim() ||
              state.name === nameCategory ||
              checkIncludesCategory(state.name, catList)
            }
            onClick={updateCategoryHandler}
          >
            Сохранить изменения
          </button>
          <button className="button" onClick={cancelChanges}>
            Отменить
          </button>
        </footer>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  const nameCategory = state.catList.find(
    (cat) => cat.catId === state.currentCat
  ).name;
  console.log(nameCategory);
  return {
    nameCategory,
    catList: state.catList,
  };
};

export default connect(mapStateToProps, null)(EditFormCategory);
