import { useState } from "react";
import { useDispatch } from "react-redux";
import { showEditForm, updateTask } from "../redux/actions";

export const Task = ({ task }) => {
  const dispatch = useDispatch();

  const [isActivDescription, setIsActiveDescription] = useState(false);

  const toggleCompleteHandler = () => {
    dispatch(updateTask(task.id, { isCompleted: !task.isCompleted }));
  };

  const toggleIsActiveDescription = () => {
    setIsActiveDescription((prev) => {
      return !prev;
    });
  };

  return (
    <div className="card">
      <header
        className={`card-header`}
        // onDoubleClick={() => dispatch(showEditForm(task.id))}
      >
        <div
          className={`image is-64x64 ${
            task.isCompleted ? "has-text-success" : "has-text-grey-lighter"
          }  icon-centered`}
          onClick={toggleCompleteHandler}
        >
          <span className="icon">
            <i className="fas fa-2x fa-check"></i>
          </span>
        </div>
        <div className={`image is-64x64 has-text-${task.color} icon-centered`}>
          <span className="icon">
            <i className={task.icon} aria-hidden="true"></i>
          </span>
        </div>
        <p className="card-header-title">{task.title}</p>
        <div className="is-flex">
          {task.description && (
            <button
              className="button image icon is-64x64"
              aria-label="more options"
              onClick={toggleIsActiveDescription}
            >
              <span className="icon">
                {isActivDescription ? (
                  <i className="fas fa-angle-down" aria-hidden="true"></i>
                ) : (
                  <i className="fas fa-angle-up"></i>
                )}
              </span>
            </button>
          )}
          <button
            className="button image is-64x64"
            onClick={() => dispatch(showEditForm(task.id))}
          >
            <span className="icon">
              <i className="far fa-edit"></i>
            </span>
          </button>
        </div>
      </header>
      {task.description && isActivDescription && (
        <div className="card-content">
          <div className="content">{task.description}</div>
        </div>
      )}
      <footer className="card-footer">
        {/* <a href="#" className="card-footer-item">
          Save
        </a>
        <a href="#" className="card-footer-item">
          Edit
        </a>
        <a href="#" className="card-footer-item">
          Delete
        </a> */}
      </footer>
    </div>
  );
};
