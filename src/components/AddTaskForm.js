import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { createTask, hideAddForm } from "../redux/actions";

const AddTaskForm = ({ currentCat }) => {
  const [state, setState] = useState({ title: "", description: "" });
  const dispatch = useDispatch();

  const changeInputHandler = (event) => {
    setState((prev) => ({
      ...prev,
      ...{ [event.target.name]: event.target.value },
    }));
  };

  const addTaskHandler = (e) => {
    e.preventDefault();
    const { title, description } = state;

    if (!title.trim()) {
      return;
    }

    const newTask = {
      title,
      description,
      id: Date.now().toString(),
      isCompleted: false,
      icon: "fas fa-2x fa-question",
      color: "light",
      catId: currentCat,
    };

    dispatch(createTask(newTask));
    dispatch(hideAddForm());
    setState({
      title: "",
      description: "",
    });
  };

  const onKeyEnterDownHandler = (event) => {
    event.key === "Enter" && addTaskHandler(event);
  };

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card" onKeyDown={onKeyEnterDownHandler}>
        <header className="modal-card-head is-justify-content-space-between">
          <p className="modal-card-title">Новая задача</p>
          <button
            className="delete is-large"
            aria-label="close"
            onClick={() => dispatch(hideAddForm())}
          ></button>
        </header>
        <section className="modal-card-body">
          <div className="box">
            <div className="field">
              <label className="label">Название задачи</label>
              <div className="control has-icons-left has-icons-right">
                <input
                  name="title"
                  className={
                    state.title.trim() ? "input is-success" : "input is-danger"
                  }
                  type="text"
                  onChange={changeInputHandler}
                  placeholder="что нужно сделать?"
                  value={state.title}
                  autoFocus
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-user"></i>
                </span>
                {state.title.trim() ? (
                  <span className="icon is-small is-right">
                    <i className="fas fa-check"></i>
                  </span>
                ) : (
                  <span className="icon is-small is-right">
                    <i className="fas fa-exclamation-triangle"></i>
                  </span>
                )}
              </div>
              {!state.title.trim() && (
                <p className="help is-danger">Поле не может быть пустым</p>
              )}
            </div>

            <div className="field">
              <label className="label">Описание задачи</label>
              <div className="control has-icons-left has-icons-right">
                <textarea
                  className="textarea mt-3"
                  name="description"
                  rows="3"
                  onChange={changeInputHandler}
                  value={state.description}
                ></textarea>
              </div>
            </div>
          </div>
        </section>
        <footer className="modal-card-foot">
          <button
            className={state.title.trim() ? "button is-success" : "button"}
            disabled={!state.title.trim()}
            onClick={addTaskHandler}
          >
            Добавить
          </button>
        </footer>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    currentCat: state.currentCat,
  };
};

// const mapDispatchToProps = {
//   createTask,
// };
export default connect(mapStateToProps /* mapDispatchToProps*/)(AddTaskForm);
