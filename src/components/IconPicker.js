export const iconsName = [
  "far fa-2x fa-address-book",
  "fas fa-2x fa-car",
  "fas fa-2x  fa-laptop-code",
  "far fa-2x fa-grin-tongue-wink",
  "fas fa-2x fa-coffee",
  "fas fa-2x fa-phone",
  "fas fa-2x fa-question",
];
export const IconPicker = ({ currentIcon, changeIcon, color }) => {
  return (
    <div className="is-flex m-5">
      {iconsName.map((icon) => {
        return (
          <button
            key={icon}
            className={`button m-3 p-5 ${
              currentIcon === icon && "is-active-button"
            }`}
            onClick={() => changeIcon(icon)}
          >
            <span className={`icon has-text-${color}`}>
              <i className={icon}></i>
            </span>
          </button>
        );
      })}
    </div>
  );
};
