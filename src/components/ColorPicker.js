export const ColorPicker = ({ currentColor, changeColor }) => {
  const bgColors = [
    "light",
    "dark",
    "primary",
    "link",
    "info",
    "success",
    "warning",
    "danger",
  ];

  return (
    <div className="is-flex is-justify-content-center">
      {bgColors.map((color) => (
        <div
          key={color}
          onClick={() => changeColor(color)}
          className={`button image is-64x64 m-1 ${
            currentColor && color === currentColor && "is-active-button"
          } has-background-${color}`}
        />
      ))}
    </div>
  );
};
