import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { addCategory, HideAddCategotyForm } from "../redux/actions";

const AddCategoryForm = ({ catList }) => {
  const [state, setState] = useState({ categoryName: "" });

  const dispatch = useDispatch();

  const changeInputHandler = (event) => {
    setState((prev) => ({
      ...prev,
      ...{ [event.target.name]: event.target.value },
    }));
  };

  const addCategoryHandler = () => {
    if (checkIncludesCategory(state.categoryName, catList)) {
      alert("Каталог с таким именем уже существует");
    } else {
      dispatch(addCategory(state.categoryName).trim());
      dispatch(HideAddCategotyForm());
    }
  };

  const checkIncludesCategory = (catName, catList) => {
    return catList.find((cat) => cat.name === catName);
  };

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card">
        <div className="modal-card-head">
          <p className="modal-card-title">Новая категория</p>
          <button
            className="delete  is-large"
            aria-label="close"
            onClick={() => dispatch(HideAddCategotyForm())}
          ></button>
        </div>
        <section className="modal-card-body">
          <label className="label">Название категории</label>
          <input
            name="categoryName"
            className={`input ${!state.categoryName.trim() && "is-danger"}`}
            type="text"
            onChange={changeInputHandler}
            value={state.categoryName}
          ></input>
          {!state.categoryName.trim() && (
            <p className="has-text-danger">
              Категорию нельзя сохранить без названия
            </p>
          )}
          {checkIncludesCategory(state.categoryName, catList) && (
            <p className="has-text-danger">
              {`Категория "${state.categoryName}" уже существует`}
            </p>
          )}
        </section>
        <div className="modal-card-foot">
          <button
            className={
              state.categoryName.trim() ? "button is-success" : "button"
            }
            disabled={
              !state.categoryName.trim() ||
              checkIncludesCategory(state.categoryName, catList)
            }
            onClick={addCategoryHandler}
          >
            Добавить категорию
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    catList: state.catList,
  };
};
export default connect(mapStateToProps)(AddCategoryForm);
